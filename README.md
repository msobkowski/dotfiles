# dotfiles

My personal configs for various software.

## Usage

Configs are divided in two directories:

- configs
- configs.unused

First one contains currently maintained set of configs.
In ``configs/scripts/.local/scripts`` directory there is a script called ``deploy_configs.sh``, that iterates over all subdirs of ``configs`` directory, checks if configs won't overwrite anything (optionally deletes), and deploys symlinks using ``stow`` utility (use at your own risk).

## Future improvements

- revamp SSH, GPG config [1] [2] [3] [4]
- Arch packages list to recreate installation
- global system configs (/etc etc.)
- gnome configs as dconf entries [5]
- gnome extensions as submodules [6]
- vim plugins as submoudles? - ditch vimplug? [7]
- firefox config in vcs [8]
- mpv
- e-mail solution (notmuch?)
- systemd user service for xcape [9]
- gnome terminal ?

## Good examples

https://sanctum.geek.nz/cgit/dotfiles.git/about/
https://github.com/skeeto/dotfiles/tree/master/bin
https://github.com/myfreeweb/dotfiles/
https://git.sr.ht/~sircmpwn/dotfiles/
https://github.com/hauleth/dotfiles

--

[1] https://eklitzke.org/using-gpg-agent-effectively
[2] https://alexcabal.com/creating-the-perfect-gpg-keypair/
[3] https://mricon.com/talks/osseu17.pdf
[4] https://riseup.net/en/security/message-security/openpgp/best-practices
[5] https://eklitzke.org/lobotomizing-gnome
[6] https://github.com/dzhou121/dotfiles/tree/master/.local/share/gnome-shell/extensions
[7] https://shapeshed.com/vim-packages/
[8] https://github.com/pyllyukko/user.js
[9] https://github.com/MaskRay/Config/blob/master/home/.config/systemd/user/xcape.service
