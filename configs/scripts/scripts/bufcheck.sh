#!/bin/sh

i=$(grep -e Dirty: /proc/meminfo | sed -n  's/.*\b\([0-9]\+\) kB/\1/p')
let "i=i/1000"
echo $i MB | figlet -l -f smslant
