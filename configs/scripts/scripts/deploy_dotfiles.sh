#!/bin/sh

# Deploy all configs to home directory. Requires GNU stow.
# WARNING: if configs already exist, they will be replaced.

DOTFILES_REPO="${DOTFILES_REPO:-/data/dotfiles}"

command -v stow 2>&1 >/dev/null || { echo "Plese install stow!"; exit 1; }

skip_pkg=0
no_ask=0

if [[ $* == *--no-ask* ]] ; then
	no_ask=1
fi

for cfg in ${DOTFILES_REPO}/configs/*; do

	echo "Importing $(basename ${cfg})..."

	if [ -e ${cfg}/.stowconf ] ; then
		. ${cfg}/.stowconf
	fi

	for file in $(find ${cfg} -maxdepth 1) ; do

		target_path="${HOME}/$(basename ${file})"
		echo "Target path: ${target_path}"

		if [ -e "${target_path}" ] || [ -h "${target_path}" ] ; then

			if [ $no_ask == "0" ] ; then

				echo "W: ${target_path} exists! Remove? [y/n]"

				read yn
				if [ ${yn} = "y" ] ; then
					rm -r ${target_path}
				else
					echo "Skiping ${target_path}."
					skip_pkg=1
					continue
				fi
			else 
				rm -r ${target_path}
			fi
		fi
	done

	if [ ${skip_pkg} == "1" ] ; then
		echo "Skiping ${cfg}."
		skip_pkg=0
		continue
	fi

	if [ -v STOW_TARGET ] ; then
		stow --dir=${DOTFILES_REPO}/configs --target=${HOME}/${STOW_TARGET} --ignore=".stowconf" $(basename ${cfg})
		unset STOW_TARGET
	else
		stow --dir=${DOTFILES_REPO}/configs --target=${HOME} $(basename ${cfg})
	fi
done
