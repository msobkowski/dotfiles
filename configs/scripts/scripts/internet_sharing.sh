#!/bin/sh

SHARE_IF=enp0s20f0u5u4u2
INTERNET_IF=enp0s31f6

# Enable network connection
nmcli connection up static_sharing ifname ${SHARE_IF}

# Enable DHCP server
sudo systemctl start dhcpd4\@${SHARE_IF}.service

# Enable packet forwarding
sudo sysctl net.ipv4.conf.${INTERNET_IF}.forwarding=1

# Configure NAT via nftables
sudo nft add table ip nat
sudo nft add chain ip nat prerouting { type nat hook prerouting priority 0 \; }
sudo nft add chain ip nat postrouting { type nat hook postrouting priority 100 \; }
sudo nft add rule nat postrouting oifname ${INTERNET_IF} masquerade
