alias clr="clear"
alias cls="clear"

alias c="clear"
alias v="vim"
alias g="git"
alias s="sudo"

alias ..="cd .."
alias wanip='dig +short myip.opendns.com @resolver1.opendns.com'
alias mnt="mount | column -t"
